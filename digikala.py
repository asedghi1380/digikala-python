import asyncio
import ujson
from aiohttp import ClientSession
import endpoints


async def run(session, start, to, concurrent_limit):
    tasks = []
    responses = []
    sem = asyncio.Semaphore(concurrent_limit)

    async def fetch(i):
        base_url = endpoints.get_product_details_link(f'{i}')
        page_number = 1
        product = {'product_id': i, 'pages': []}
        while True:
            url = base_url + '?page=' + str(page_number)
            print(url)
            async with session.get(url) as response:
                response = await response.json(encoding="utf-8")
                sem.release()
                if response['status'] == 200 and response['data']['pager']['total_items'] > 0:
                    product['pages'].append(response['data']['comments'])
                    if response['data']['pager']['current_page'] != response['data']['pager']['total_pages']:
                        page_number += 1
                        continue
                    else:
                        break
                else:
                    break
        responses.append(product)

    for i in range(start, to):
        await sem.acquire()
        task = asyncio.ensure_future(fetch(i))
        task.add_done_callback(tasks.remove)
        tasks.append(task)

    await asyncio.wait(tasks)
    print("total_responses: {}".format(len(responses)))
    return responses


async def main(start, to, concurrent_limit):
    async with ClientSession() as session:
        responses = await asyncio.ensure_future(run(session, start, to, concurrent_limit))
        print(start, to)
        f = open(f"data/data{round(to / 10)}.json", "w")
        json_object = ujson.dumps(responses, ensure_ascii=False)
        f.write(json_object)
        f.close()
        json_object = None
        responses = None
        return


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    page_from = 1
    page_to = 8252122
    every_page = 2000
    concurrent_limit = 2000
    for n in range(page_from, page_to):
        start = (n - 1) * every_page
        to = n * every_page
        loop.run_until_complete(main(start, to, concurrent_limit))
    loop.close()
