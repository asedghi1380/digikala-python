#!/usr/bin/env python3

from aiohttp import ClientSession, TCPConnector
import asyncio
import sys

import endpoints


async def fetch(url, session):
    async with session.get(url) as response:
        return await response.read()


limit = 1000


async def print_when_done():
    async with ClientSession(connector=TCPConnector(limit=limit)) as session:
        tasks = (fetch(endpoints.get_product_details_link(f'{i}'), session) for i in range(r))
        print(tasks)
        for res in asyncio.as_completed(tasks):
            await res


r = 1000

loop = asyncio.get_event_loop()
loop.run_until_complete(print_when_done())
loop.close()
