from asyncio import TimeoutError

import backoff
import requests

import endpoints
import httpx
import asyncio


def fatal_code(e):
    print('back of')
    return 400 <= e.response.status_code < 500


@backoff.on_exception(backoff..,
                      [TimeoutError, httpx.TimeoutException],
                      max_time=300,

                      giveup=fatal_code)
async def get_async(url):
    async with httpx.AsyncClient(timeout=30.0) as client:
        print((await client.get(url)).json())


async def launch():
    await asyncio.gather(*[get_async(endpoints.get_product_details_link(f'{x}')) for x in range(1000)])


asyncio.run(launch())
