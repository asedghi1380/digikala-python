import urllib

# PRODUCT_URL = 'https://r8tyytmt4e.execute-api.eu-west-2.amazonaws.com/ProxyStage/v1/product/1/'
PRODUCT_URL = 'https://365el9wuz1.execute-api.ap-southeast-2.amazonaws.com/ProxyStage/v1/product/%s/comments/'


def get_product_details_link(id):
    return PRODUCT_URL % urllib.parse.quote_plus(id)

